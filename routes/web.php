<?php

use Illuminate\Support\Facades\Auth;

Route::get('/', 'HomeController@index')->name('home.index');

Route::group(['prefix' => 'admin'], function(){
    Auth::routes();
    
    Route::group(['middleware' => ['auth']], function () {
        // Route::get('dashboard', 'DashboardController@index')->defaults('_config',[
        //     'view'=>'admin.index'
        // ])->name('dashboard');

        Route::resource('form', 'FormsController');
        Route::post('form/checkunique', 'FormsController@checkunique')->name('form.unique');
        Route::resource('questions', 'QuestionController');
        Route::resource('submission', 'SubmissionController');
        Route::get('submission/export/{id}', 'SubmissionController@export')->name('submission.export');
        Route::get('submission/clear/{id}', 'SubmissionController@clearres')->name('submission.clear');
    });
});

Route::get('form/{link}', 'SurveyController@index')->name('survey.index');

Route::post('form/save', 'SurveyController@store')->name('survey.store');

Route::post('form/get', 'SurveyController@getans')->name('survey.get');
