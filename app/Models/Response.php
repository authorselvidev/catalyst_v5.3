<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Form;
use App\Models\Questions;
use App\Models\UserSubmit;

class Response extends Model
{
    protected $guarded = [];

    public function form(){
        return $this->belongsTo(Form::class);
    }

    public function user(){
        return $this->belongsTo(UserSubmit::class,'usersubmit_id');
    }

    public function question(){
        return $this->belongsTo(Questions::class);
    }
}
