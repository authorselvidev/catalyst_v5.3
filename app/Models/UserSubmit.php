<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Form;

class UserSubmit extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function form(){
        return $this->belongsTo(Form::class);
    }
}
