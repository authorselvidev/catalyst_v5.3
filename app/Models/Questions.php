<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Form;
use App\Models\Response;


class Questions extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function form(){
        return $this->belongsTo(Form::class);
    }

    public function responses(){
        return $this->hasMany(Response::class,'question_id');
    }
}
