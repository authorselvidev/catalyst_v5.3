<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Questions;
use App\Models\Response;
use App\Models\UserSubmit;

class Form extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function questions(){
        return $this->hasMany(Questions::class);
    }

    public function responses(){
        return $this->hasMany(Response::class);
    }

    public function users(){
        return $this->hasMany(UserSubmit::class);
    }
}
