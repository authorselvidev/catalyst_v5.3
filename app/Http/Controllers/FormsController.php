<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;
class FormsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=array();
        $data['forms'] = Form::all()->reverse();
        $data['type']="create";
        return view('admin.forms.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $data=array();
        $data['type']="create";
        return view('admin.forms.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $input['status']=trim($request->status);
        $form = Form::create($input);

        return redirect()->route('questions.index',['form_id'=>$form->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=array();
        $data['type']="edit";
        $data['form'] = Form::find($id);
        return view('admin.forms.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $form=Form::find($id);
        $form->name=$input['name'];
        $form->time=$input['time'];
        $form->link=$input['link'];
        $form->status=trim($input['status']);
        $form->save();
        if($form->users->where('status',1)->count()<1){
            return redirect()->route('questions.edit',[$form->id, 'backto'=>1]);
        }
        return redirect()->route('form.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Form::find($id)->delete();
        return redirect()->route('form.index');
    }

    public function checkunique(Request $request){
        return ['status' => !Form::where('link',$request->link)->first()];
    }
}
