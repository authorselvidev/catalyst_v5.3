<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    private $_config;
    public function __construct(){
        $this->_config = request('_config');
    }

    public function index(){
        return view($this->_config['view']);
    }
}
