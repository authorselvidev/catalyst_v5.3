<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Questions;
use App\Models\Form;
class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $form = Form::find($request->form_id);

        if(!$this->checkForm($form)) return redirect()->route('form.index');

        return view('admin.questions.create', [
            'form'=> $form,
            'type'=>'create'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $form = Form::find($request->form_id);
        if(!$this->checkForm($form)) return redirect()->route('form.index');

        Questions::where('form_id', $request->form_id)->forceDelete();
        if(!empty($request->input)){
            $questions = $input['input'];
            ksort($questions);

            foreach($questions as $key => $question){
                if($question['question_type'] != 4){
                    Questions::create([
                        'question_text'=> $question['question_text'] ?? '',
                        'question_type'=> $question['question_type'],
                        'placeholder_text'=> $question['placeholder_text'] ?? '',
                        'form_id' => $request->form_id,
                        'required'=> (!empty($question["required"]) && $question["required"]=='on')?1:0,
                    ]);
                }else{
                    $file = $request->input[$key]['file'];

                    $file_extension=$file->getClientOriginalExtension();
                    $img_name =uniqid().'.'.$file_extension;
                    $file->move(public_path('img/questions/'), $img_name);

                    Questions::create([
                        'question_text'=> $img_name,
                        'question_type'=> $question['question_type'],
                        'placeholder_text'=> $question['placeholder_text'] ?? '',
                        'form_id' => $request->form_id,
                        'required'=> (!empty($question["required"]) && $question["required"]=='on')?1:0,
                    ]);
                }
            }
        }else{
            return redirect()->back();
        }
        return redirect()->route('form.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = Form::find($id);
        if(!$this->checkForm($form)) return redirect()->route('form.index');

        $question = Questions::where('form_id', $id)->get();
        return view('admin.questions.create', [
            'form'=>$form,
            'questions' =>json_encode($question->toArray()),
            'type'=>'edit'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $form = Form::find($request->form_id);
        if(!$this->checkForm($form)) return redirect()->route('form.index');

        Questions::where('form_id', $request->form_id)->delete();
        if(!empty($request->input)){
            $questions = $input['input'];
            ksort($questions);

            foreach($questions as $key => $question){
                if($question['question_type'] != 4){
                    if(!empty($question['question_id'])){
                        Questions::withTrashed()->where('id',$question['question_id'])->restore();
                        Questions::where('id',$question['question_id'])->update([
                            'question_text'=> $question['question_text'] ?? '',
                            'question_type'=> $question['question_type'],
                            'placeholder_text'=> $question['placeholder_text'] ?? '',
                            'form_id' => $request->form_id,
                            'required'=> (!empty($question["required"]) && $question["required"]=='on')?1:0,
                        ]);
                    }else{
                        Questions::create([
                            'question_text'=> $question['question_text'] ?? '',
                            'question_type'=> $question['question_type'],
                            'placeholder_text'=> $question['placeholder_text'] ?? '',
                            'form_id' => $request->form_id,
                            'required'=> (!empty($question["required"]) && $question["required"]=='on')?1:0,
                        ]);
                    }
                }else{
                    $img_name = $request->input[$key]['file_name'] ?? '';

                    if(isset($request->input[$key]['file'])){
                        $file = $request->input[$key]['file'];
                        $file_extension=$file->getClientOriginalExtension();
                        $img_name =uniqid().'.'.$file_extension;
                        $file->move(public_path('img/questions/'), $img_name);
                    }

                    if(!empty($question['question_id'])){
                        Questions::withTrashed()->where('id',$question['question_id'])->restore();
                        Questions::where('id',$question['question_id'])->update([
                            'question_text'=> $img_name,
                            'question_type'=> $question['question_type'],
                            'placeholder_text'=> $question['placeholder_text'] ?? '',
                            'form_id' => $request->form_id,
                            'required'=> (!empty($question["required"]) && $question["required"]=='on')?1:0,
                        ]);
                    }else{
                        Questions::create([
                            'question_text'=> $img_name,
                            'question_type'=> $question['question_type'],
                            'placeholder_text'=> $question['placeholder_text'] ?? '',
                            'form_id' => $request->form_id,
                            'required'=> (!empty($question["required"]) && $question["required"]=='on')?1:0,
                        ]);
                    }
                }
            }
        }else{
            return redirect()->back();
        }
        return redirect()->route('form.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function checkForm($form){        
        if($form->users->where('status',1)->count()>0){
            return false;
        }
        return true;
    }
}
