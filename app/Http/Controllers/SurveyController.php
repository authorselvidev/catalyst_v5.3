<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;
use App\Models\Response;
use App\Models\User;
use App\Models\UserSubmit;
use Mail;

class SurveyController extends Controller
{
    public function index($link)
    {
        $form = Form::where('status','1')->where('link', $link)->first();
        return view('user.forms.index',[
            'form' => $form
        ]);
    }

    public function store(Request $request)
    {
        $form_id = $request->form_id;
        $user_data = $request->user_data;
        $user_data['form_id'] = $request->form_id;

        $form = Form::find($form_id);
        $data['form'] = Form::find($form_id);

        $user_submit = UserSubmit::where('email', $user_data['email'])->where('form_id', $form_id)->where('status', 0)->first();
        if(empty($user_submit)){
            $user_submit = UserSubmit::create($user_data);
        }else{
            $user_submit->update($user_data);
        }
        

        $data['answers'] = $request->ans ?? [];
        $data['user_data'] = $user_data;

        $response = Response::where('form_id', $form_id)->where('usersubmit_id', $user_submit->id)->update([
            'response'=>'',
        ]);

        foreach($request->ans ?? [] as $ans){
            $response = Response::where('form_id', $form_id)->where('question_id', $ans['qid'])->where('usersubmit_id', $user_submit->id)->first();
            if (!empty($response)) {
                $response->update([                    
                    'response'=>$ans['ans'],
                ]);
            }else{
                $response = Response::create([
                    'form_id' => $form_id,
                    'question_id'=> $ans['qid'],
                    'usersubmit_id'=> $user_submit->id,
                    'response'=>$ans['ans'],
                ]);
            }
        }

        if($request->action == 'submit'){
            session()->put("form_".$form_id, true);
            $admins = User::where('role',1)->pluck('email')->toArray();   
            array_push($admins, 'saneesh@ascendus.com');
            Mail::send('emails.response', $data, function ($message) use ($user_data, $admins){
                $message->from('info@ascendus.com', 'Catalyst');
                $message->to(['tvrlsblr@gmail.com'])->subject('Catalyst - New form Submition');
            });    
        }

        return ['status'=>true];
    }

    public function getans(Request $request){
        $user_submit = UserSubmit::where('email', $request->email)->where('form_id', $request->form_id)->where('status', 0)->first();        

        if(empty($user_submit)){
            return ['success'=>false];
        }

        $response = Response::where('form_id', $request->form_id)->where('usersubmit_id', $user_submit->id)->get();

        return [
            'success'=>true,
            'user_submit'=> $user_submit->toArray() ?? [],
            'qst'=> $response->toArray() ?? [],
        ];
    }
}
