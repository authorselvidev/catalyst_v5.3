<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;
use Maatwebsite\Excel\Facades\Excel;

class SubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $form = Form::where('id', $request->form)->firstOrFail();

        return view('admin.submits.index',[
            'form' => $form
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export($id){

      Excel::create('Form_submitions_-'.$id.'-'.strtotime("now"), function($excel) use ($id)  {
         $excel->sheet('Sheet 1', function($sheet) use ($id) {
               $sheet->freezeFirstRow();
             $sheet->loadView('admin.submits.table')
             ->with('form', Form::find($id));

           });
        })->download('xlsx');
        // return Excel::download(new SubmitExport($id), 'Form_submitions_'.$id.'.xlsx');
    }

    public function clearres($id)
    {
        $form = Form::find($id);
        $form->responses()->delete();
        $form->users()->delete();
        return redirect()->back();
    }
}
