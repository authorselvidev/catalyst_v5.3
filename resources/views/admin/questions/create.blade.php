@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <span class="d-flex align-items-center">
                <a href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i></a>
                <h1 class="h3 mb-0 ml-2 text-gray-800">@if($type=="create")Create @else Update @endif Questions</h1>
            </span>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-4">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> <br> <!-- fieldsets -->
                    <div class="card-body px-5">

                        @if ($type=="create")
                            {!! Form::open(['route' => 'questions.store','method'=>'POST','id'=>'form_validate','enctype' =>'multipart/form-data']) !!}
                        @else
                            {!! Form::open(['route' => ['questions.update',$form->id],'method'=>'PATCH','id'=>'form_validate', 'enctype' =>'multipart/form-data']) !!}
                        @endif

                            <input type="hidden" name="form_id" value="{{$form->id}}">
                            <div class="row mb-5 px-2 py-3 d-flex justify-content-center">
                                <div class="col-8  rounded form_container">

                                </div>
                                <div class="col-3 form-add d-flex align-items-end py-5">
                                    <ul class="list-group border border-primary form_list_items">
                                        <li class="list-group-item active"><h5>Add Form Elements</h5></li>
                                        <li class="list-group-item"><a href="#" class="add-ele" data-type='1'><i class="fas fa-plus"></i> Text</a></li>
                                        <li class="list-group-item"><a href="#" class="add-ele" data-type='2'><i class="fas fa-plus"></i> Textarea</a></li>
                                        <li class="list-group-item"><a href="#" class="add-ele" data-type='3'><i class="fas fa-plus"></i> Paragraph</a></li>
                                        <li class="list-group-item"><a href="#" class="add-ele" data-type='4'><i class="fas fa-plus"></i> Image</a></li>
                                        <li class="list-group-item"><a href="#" class="add-ele" data-type='5'><i class="fas fa-plus"></i> Pagebreak</a></li>
                                    </ul>
                                </div>
                            </div>
                            <button class="btn btn-primary float-right ml-2">Save</button>
                            @if(isset(request()->backto))
                                <a href="{{route('form.edit',$form->id)}}" class="btn btn-primary float-right">Back</a>
                            @else
                                <a href="{{route('form.index')}}" class="btn btn-primary float-right">Back</a>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push("scripts")
    <link rel="stylesheet" href="{{asset('css/form-builder.css')}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.2/tinymce.min.js" referrerpolicy="origin"></script>

    <script type="text/javascript">
            $(function(){
                let $count = 1;
                let path = '{{asset("img/questions")}}/';
                let form_question = form_questions = {!! $questions ?? '[]' !!};

                $('body').on('click', '.add-ele', function(e){
                    e.preventDefault();
                    addQuestion($(this).data('type'));
                });

                function addQuestion(type=1, data=null){
                    const ele = $('.form_container');
                    let html = $(`
                        <div class="quest_ele rounded border">
                            <span class="close-btn text-danger">&times;</span>
                            <h5 class="qst_type">Text</h5>
                            <input type="hidden" name="input[${$count}][question_type]" value="${type}">
                            <div class="form-group">
                                <textarea name="input[${$count}][question_text]" id="question_${$count}" rows="3"  class="form-control required" placeholder="Enter question here">${ ((data!=null)?(data['question_text']??''):'') }</textarea>
                            </div>
                            <div class="form-group">
                                <input name="input[${$count}][placeholder_text]" type="text" class="form-control" placeholder="Enter placeholder" value="${((data!=null)?(data['placeholder_text']??''):'')}">
                            </div>

                            <div class="form-check d-flex justify-content-end">
                                <span>
                                    <input type="checkbox" name="input[${$count}][required] value="1" id="check_box${$count}" class="form-check-input" ${((data==null)?'checked':(data['required']==1)?'checked':'')}>
                                    <label class="form-check-label" for="check_box${$count}">Required</label>
                                </span>
                            </div>
                        </div>
                    `);

                    switch(type){
                        case 1:
                            html.find('.qst_type').text('Text');
                            break;
                        case 2:
                            html.find('.qst_type').text('Textarea');
                            break;
                        case 3:
                            html = $(`
                                <div class="quest_ele  rounded border">
                                    <span class="close-btn text-danger">&times;</span>
                                    <h5 class="qst_type">Paragraph</h5>
                                    <input type="hidden" name="input[${$count}][question_type]" value="${type}">
                                    <div class="form-group">
                                        <textarea name="input[${$count}][question_text]" id="para_${$count}" rows="3"  class="form-control required" placeholder="Enter question here">${ ((data!=null)?(data['question_text']??''):'') }</textarea>
                                    </div>
                                </div>
                            `);
                            break;
                        case 4:
                            html = $(`
                                <div class="quest_ele  rounded border">
                                    <span class="close-btn text-danger">&times;</span>
                                    <h5 class="qst_type">Image</h5>
                                    <div class="form-group">
                                        <input type="hidden" name="input[${$count}][question_type]" value="${type}">
                                        <input type="file" name="input[${$count}][file]" class="form-control img-thumb">
                                    </div>
                                </div>
                            `);

                            if(data!=null){
                                html.find('.form-group').append(`<img class="imgThumb img-enlargeable" src="${path+data['question_text']}">`);
                                html.find('.form-group').append(`<input type="hidden" name="input[${$count}][file_name]" value="${data['question_text']}">`);
                            }

                            break;
                        case 5:
                            html = $(`
                            <div class="quest_ele rounded border">
                                <span class="close-btn text-danger">&times;</span>
                                <h5 class="qst_type">Pagebreak</h5>
                                <hr>
                                <div class="form-group">
                                    <input type="hidden" name="input[${$count}][question_type]"  value="${type}">
                                </div>
                            </div>`);
                            break;
                        default:
                            alert('Invalid Option');
                            break;
                    }

                    if(data!=null){
                        $(html).append(`<input type="hidden" name="input[${$count}][question_id]" value="${data['id']}">`);
                        console.log(html);
                    }

                    ele.append($(html));

                    if(data==null){
                        $("html, body").animate({
                            scrollTop: $(document).height()
                        }, 1000);
                    }

                    tinymce.init({
                        selector: `#para_${$count}`,
                        plugins: 'image imagetools media wordcount save fullscreen code',
                        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent  | removeformat | code',
                        image_advtab: true,
                        relative_urls : false,
                        remove_script_host : false,
                        convert_urls : true,
                    });

                    $count++;
                }

                $('body').on('click', '.close-btn', function(e){
                    $(this).closest('.quest_ele').remove();
                });

                form_question.forEach(function(val){
                    addQuestion(val['question_type'], val);
                });

                $('body').on('change','.img-thumb',function(event) {
                    let input = event.target;
                    let reader = new FileReader();
                    reader.onload = function(){
                        let dataURL = reader.result;
                        let output = $(`<img class='imgThumb'>`).hide();
                        output.attr('src', dataURL);
                        $(input).next('.imgThumb').remove();
                        $(input).after(output);
                        output.fadeIn();
                        output.addClass('img-enlargeable');
                    };
                    reader.readAsDataURL(input.files[0]);
                });

                $('body').on('click','.img-enlargeable', function(){
                    var src = $(this).attr('src');
                    var modal;
                    function removeModal(){
                        modal.remove();
                        $('body').off('keyup.modal-close');
                    }
                    modal = $('<div>').css({
                        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
                        backgroundSize: 'contain',
                        width:'100%', height:'100%',
                        position:'fixed',
                        zIndex:'10000',
                        top:'0', left:'0',
                        cursor: 'pointer'
                    }).click(function(){
                        removeModal();
                    }).appendTo('body');


                });

                $('body').on('keyup.modal-close', function(e){
                    if(e.key==='Escape'){
                        removeModal();
                    }
                });


                $('body').on('submit', '#form_validate', function(e){
                    $('.err_form').remove();
                    let validate = true;
                    $(this).find('.required').each(function(){
                        if(!($(this).val())){
                            validate = false;
                            $(this).after('<span class="text-danger err_form">This field is required</span>');
                        }
                    });
                    if(!validate) e.preventDefault();
                });
            });
    </script>
@endpush
