@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <span class="d-flex align-items-center">
                <a href="{{ route('form.index') }}"><i class="fas fa-arrow-left"></i></a>
                <h1 class="h3 mb-0 ml-2 text-gray-800"> @if($type=="create")Create @else Update @endif form</h1>
            </span>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-4 step1">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> <br>
                    <div class="card-body px-5">
                            @if ($type=="create")
                            {!! Form::open(['route' => 'form.store','method'=>'POST','id'=>'form_validate']) !!}
                          @else
                            {{ Form::model($form,['method' => 'PATCH', 'route' =>['form.update',$form->id],'id'=>'form_validate'] ) }}
                          @endif
                            <div class="col-md-6 mx-auto column">
                                <div class="form-group">
                                    <label for="form_name"><strong>Form name:</strong></label>
                                    {!! Form::text('name',null,['class'=>'form-control','id'=>'form_name']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="form_timer"><strong>Timer(minutes):</strong></label>
                                    {!! Form::number('time',null,['class'=>'form-control','id'=>'form_timer']) !!}
                                </div>

                                <div class="form-group">
                                    <label for="form_link"><strong>URL Link:</strong></label>
                                    <span class="d-flex align-items-start">
                                        <span class="border rounded-left" style="padding:6px 5px;">{{\URL::to('/')}}/form/</span>
                                        <span style="width: 100%;">
                                            {!! Form::text('link',null,['class'=>'form-control','id'=>'form_link']) !!}
                                        </span>
                                    </span>
                                </div>
                                <div class="form-group d-flex align-items-center toggle_form_status" >
                                    <label for="message-text" class="col-form-label">Status:</label>
                                    <input type="hidden" value="@if(isset($form)) {{$form->status}} @else 1 @endif" name="status" id="form_status">
                                    <a href="#" class="text-primary ml-1 d-flex" style="font-size: 1.2rem;"><i class="fas fa-toggle-{{ ([1=>'on',0=>'off'][$form->status ?? 1])}}"></i></a>
                                </div>
                                <button type="submit" class="btn btn-primary float-right"> @if($type=="create")Create @else Update @endif</button>
                                <a href="{{route('form.index')}}" class="btn btn-primary float-right mr-2">Back</a>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push("scripts")
    <link rel="stylesheet" href="{{asset('css/create-form.css')}}">
    <script type="text/javascript">
        $(function(){
            let create = @if($type=="create")true @else false @endif;
            $('body').on('click', '.toggle_form_status>a', function(e){
                let ele = $(this).closest('.toggle_form_status');
                const toggle_value = ele.find('#form_status');
                const toggle_style = ele.find('i');
                stat = (toggle_value.val()==0)?1:0;
                toggle_value.val(stat);
                if(stat==1){
                    toggle_style.removeClass('fa-toggle-off').addClass('fa-toggle-on');
                }else if(stat==0){
                    toggle_style.removeClass('fa-toggle-on').addClass('fa-toggle-off');
                }
                e.preventDefault();
            });

            $('body').on('submit', '#form_validate', function(e){
                e.preventDefault();
                var this_form = $(this);
                $('.err_form').remove();
                let validate = true;
                $(this).find('input, textarea').each(function(){
                    if(!($(this).val())){
                        validate = false;
                        $(this).after('<span class="text-danger err_form">This field is required</span>');
                    }
                });

                let ele = $(this).find('#form_link');
                let link = ele.val();

                if(create){
                    $.ajax({
                        url:"{{route('form.unique')}}",
                        method:'POST',
                        data:{
                            "_token": "{{ csrf_token() }}",
                            "link":link,
                        },
                        success:function(res){
                            if(!res['status']){
                                ele.after('<span class="text-danger err_form">Link already used!</span>');
                            }else if(validate == true){
                                this_form.unbind(e);
                                this_form.submit();
                            }
                        }
                    });
                }else{
                    if(validate == true){
                        this_form.unbind(e);
                        this_form.submit();
                    }
                }
            });
        });
    </script>
@endpush
