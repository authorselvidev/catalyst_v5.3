@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Manage Forms</h1>
            <a href="{{route('form.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus"></i> Create Form</a>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-4">
                    <div
                        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Forms</h6>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Time(minutes)</th>
                                        <th>Link</th>
                                        <th>Submits Count</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Time</th>
                                        <th>Link</th>
                                        <th>Submits Count</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($forms ?? [] as $form)
                                    <tr>
                                        <td>{{$form->name}}</td>
                                        <td>{{$form->time}}</td>
                                        <td>
                                            <span>{{\URL::to('/')}}/form/{{$form->link}}</span>
                                            <span class="fa fa-clone btn-copy" aria-hidden="true" data-clipboard-text="{{\URL::to('/')}}/form/{{$form->link}}"></span>
                                        </td>
                                        <td>{{$form->users->where('status',1)->count()}}</td>
                                        <td>{{[0=>"Deactive", 1=>"Active"][$form->status] ?? 'Unknown'}}</td>
                                        <td class="d-flex flex-column align-items-center">
                                            <a href="{{route('form.edit',[$form->id])}}" class="btn btn-primary btn-sm mb-2"><i class="fas fa-cog"></i> Edit Form</a>
                                            <a href="#" class="btn btn-danger btn-sm mb-2 delete_method" data-url="{{route('form.destroy',[$form->id])}}"><i class="fas fa-trash-alt"></i> Delete Form</a>
                                            @if($form->users->where('status',1)->count()<1)
                                                <a href="{{route('questions.edit',$form->id)}}" class="btn btn-primary mb-2 btn-sm"><i class="fas fa-edit"></i> Edit Questions </a>
                                            @else
                                                <a href="{{route('submission.index',['form'=>$form->id])}}" class="btn btn-primary mb-2 btn-sm"><i class="fas fa-comments"></i> Responses </a>
                                                <a href="{{route('submission.clear',['form'=>$form->id])}}" class="btn btn-primary mb-2 btn-sm delete_response"><i class="fas fa-trash-alt"></i> Delete Response </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/manage-forms.js')}}"></script>
    <style>
        td a{
            width:150px ;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
    <script>
        $(function(){

            $('#dataTable').DataTable({
                "autoWidth": false,
                "bSort" : false
            });

            $('.delete_response').click(function(e){
                if(!confirm('Do you like to delete responses?')){
                    e.preventDefault();
                }
            });

            $('.delete_method').click(function(){
                if(confirm('Do you like to delete this form?')){
                    $.ajax({
                        url:$(this).data('url'),
                        method:"POST",
                        data:{
                            "_method":"DELETE",
                            '_token':'{{csrf_token()}}'
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }                
            });

            const afterCopy = function(e){
            var target = $(e.trigger);
            $(e.trigger).append($('<span class="copied-tip">Copied</span>').hide().fadeIn(100));
                setTimeout(function(){
                    target.find('.copied-tip').fadeOut(200, function(){$(this).remove()});
                },2000);
            }

            var clipboard1 = new ClipboardJS('.btn-copy');

            clipboard1.on('success', afterCopy);
        })

    </script>
     <style type="text/css">
        .btn-copy, .inv-copy{
            cursor: pointer;
            display: inline-block;
            margin-left: 5px;
            color: #0995CD;
            position: relative;
        }

        .copied-tip{
            position: absolute;
            font-size: .8rem;
            padding: 6px 9px;
            top: -29px;
            color: #EEE;
            left: 50%;
            transform: translateX(-50%);
            background-color: rgba(0, 0, 0,.7);
            font-family: arial, sans-serif;
            border-radius: 3px;
            z-index: 1000;
            box-shadow: 1.5px 1.5px 2px rgba(0, 0, 0, .3);
        }
    </style>
@endpush
