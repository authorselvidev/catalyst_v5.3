<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <?php $count = 1; ?>
            @foreach ($form->questions as $question)
                @if(in_array($question->question_type, [1,2]))
                    <th>Q{{$count++}}</th>
                @endif
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($form->users->where('status',1)->reverse() as $user)
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            @foreach ($form->questions as $question)
                @if(in_array($question->question_type, [1,2]))
                    <td>{{$question->responses->where('usersubmit_id', $user->id)->first()->response ?? '-'}}</td>
                @endif
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
