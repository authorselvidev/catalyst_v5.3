<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="x-apple-disable-message-reformatting">
    <title></title>
    <!--[if mso]>
        <noscript>
            <xml>
            <o:OfficeDocumentSettings>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
        </noscript>
    <![endif]-->
    <style>
        table,td,div,h1,p{
            font-family: Arial, sans-serif;
        }


    </style>
</head>

<body style="margin:0;padding:0;">
    <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
        <tr>
            <td align="center" style="padding:0;">
                <table role="presentation" style="width:100%;max-width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
                    <tr>
                        <td align="center" style="padding:10px 0;background:#0e99ea;">
                            <h1 style="color:#FFF">{{$form->name}}</h1>
                            <h3 style="color:#FFF">T.V Rao Learning System</h3>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:36px 30px 42px 30px;">
                            <table role="presentation" style="width:100%;border-collapse:collapse;border-spacing:0;margin-bottom:15px">
                                <tr>
                                    <td style="padding:8px 5px;color:rgb(40, 40, 40)">Name:</td>
                                    <td style="padding:8px 5px;color:rgb(40, 40, 40)">{{$user_data['name'] ?? ''}}</td>
                                </tr>
                                <tr>
                                    <td style="padding:8px 5px;color:rgb(40, 40, 40)">Email:</td>
                                    <td style="padding:8px 5px;color:rgb(40, 40, 40)">{{$user_data['email'] ?? ''}}</td>
                                </tr>
                            </table>



                             <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">

                                <tr>
                                    <td style="padding:0 0 5px 0;color:#153643;">
                                        <h2 style="font-size:20px;margin:0 0 20px 0;font-family:Arial,sans-serif;">Submited questions</h2>
                                    </td>
                                </tr>
                                @foreach ($answers ?? [] as $ans)
                                    <tr>
                                        <td>
                                            <h6 style="font-size:16px;margin:0 0 10px 0;font-family:Arial,sans-serif;color:#153643;">{{App\Models\Questions::where('id', $ans['qid'])->value('question_text')}}</h6>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;color:rgb(40, 40, 40)">{{$ans['ans']}}</p>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:10px;background:#0e99ea; color:#FFF;text-align:center">
                            <p><span>Copyright &copy; {{ date('Y') }} - Catalyst</span></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
