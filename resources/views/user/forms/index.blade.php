@extends('layouts.survey')

@section('title', $form->name??'')

@section('content')
    <div class="main_container">    
        <div class="header_content">
            <span style="align-items: flex-start;">
                <span style="font-size: 1.8rem;text-transform: uppercase;">Catalyst</span>
                <span style="font-size: 1rem;">by TV RAO Learning Systems</span>                
            </span>
            
            <span style="align-items: flex-end;">
                <span style="font-size: 1.7rem">Welcome to the Development Centre</span>
                <span style="font-size: 1rem;">{{$form->name?? ''}}</span>
            </span>
        </div>

        
        <div class="main-bothside">

            @if(empty($form))
                <p align="center" style="font-size: 1.5rem;padding: 30px 0;color: #ff4444;"><strong>Page Not Found!</strong></p>
            @endif
        @if(!empty($form))
            <?php
                $qst_types = array_values($form->questions->pluck('question_type')->toArray());
                $hasform= in_array(1, $qst_types) || in_array(2, $qst_types);
                $step = ($hasform)?2:1;
            ?>
            @if(session()->get("form_".$form->id))
                <div class="form-mid-w3ls form-full text-center survey_end">
                    <h3 class="text-center">Thank You!</h3>
                    <p>You already Submited this form.</p>
                </div>
            @else

                @if($form->time!=0)
                    <div class="d-flex justify-content-end timer_container mb-3">
                        <strong>Time left: <span id="timer"></span></strong>
                    </div>
                @endif

                    {!! Form::open(['route' => 'survey.store','Method'=>'POST','class'=>'my-login-validation']) !!}

                    @if($hasform)
                        <div class="form-group steps step_1">
                            <div class="form-mid-w3ls">
                                <strong><span class="d-inline-block mb-1">First name<span class="text-danger">*</span></span></strong>
                                <input type="text" name="user[name]" class="form-control required" id="fname" placeholder="Enter your first name" >
                            </div>

                            <div class="form-mid-w3ls">
                                <strong><span class="d-inline-block mb-1">Last name</span></strong>
                                <input type="text" name="user[name]" class="form-control" id="lname" placeholder="Enter your last name" >
                            </div>
                        </div>

                        <div class="form-group steps step_1">
                            <div class="form-mid-w3ls form-full">
                                <strong><span class="d-inline-block mb-1">Email<span class="text-danger">*</span></span></strong>
                                <input type="email" name="user[email]"  class="form-control required"  id="useremail" placeholder="Enter your email" >
                            </div>
                        </div>
                    @endif

                    @foreach($form->questions as $question)
                        @if($question->question_type == 1) <!-- Text -->
                            <div class="form-group mb-4 steps step_{{$step}}" data-qst="{{$question->id}}">
                                <div class="form-mid-w3ls form-full">
                                    <strong><span class="d-inline-block mb-1">{{$question->question_text}}@if($question->required==1)<span class="text-danger">*</span>@endif</span></strong>
                                    <input type="text" name="ans[qst_{{$question->id}}]" id="qst_{{$question->id}}" class="form-control @if($question->required==1) required @endif ans" placeholder="@if(empty($question->placeholder_text)){{'Type here...'}}@else{{$question->placeholder_text}}@endif">
                                </div>
                            </div>
                        @elseif($question->question_type == 2) <!-- Textarea -->
                            <div class="form-control-w3l mb-4 steps step_{{$step}}"  data-qst="{{$question->id}}">
                                <strong><span class="d-inline-block mb-1">{{$question->question_text}}@if($question->required==1)<span class="text-danger">*</span>@endif</span></strong>
                                <textarea name="ans[qst_{{$question->id}}]" id="qst_{{$question->id}}" class="form-control @if($question->required==1) required @endif ans" placeholder="@if(empty($question->placeholder_text)){{'Type here...'}}@else{{$question->placeholder_text}}@endif"></textarea>
                            </div>
                        @elseif($question->question_type == 3) <!-- Paragraph -->
                            <div class="form-control-w3l mb-4 steps step_{{$step}}" data-qst="{{$question->id}}">
                                <p>{!! $question->question_text !!}</p>
                            </div>
                        @elseif($question->question_type == 4) <!-- Image -->
                            <div class="form-control-w3l mb-4 text-center steps step_{{$step}}" data-qst="{{$question->id}}">
                                <img src="{{asset("img/questions")}}/{{$question->question_text}}" class="img-fluid px-3">
                            </div>
                        @elseif($question->question_type == 5) <!-- Pagebreak -->
                            <?php $step++; ?>
                        @endif
                    @endforeach

                    <div class="form-mid-w3ls form-full text-center survey_end" style="display: none">
                        <h3 class="text-center">Thank You!</h3> 
                        <p>Your submission has been received.</p>
                    </div>

                    <div class="form-mid-w3ls form-full text-center survey_pause" style="display: none">
                        <h3 class="text-center">Thank You!</h3>
                        <p>You can resume later with this form link.</p>
                    </div>

                    <div class="d-flex justify-content-between nav">
                        <div class="d-flex flex-start">
                            <button class="back btn btn-primary" style="display: none">Back</button>
                        </div>
                        <div class="d-flex flex-end">
                            <button class="save btn btn-primary" style="margin-left: auto;margin-right:5px;"><span>Save</span></button>
                            <button class="next btn btn-primary" style="margin-left: auto">Next</button>                        
                        </div>
                    </div>

                    <div class="justify-content-between nav_submit" style="display: none">
                        <div class="d-flex flex-start">
                            <button class="back btn btn-primary" style="display: none">Back</button>
                        </div>
                        <div class="d-flex flex-end">
                            <button class="save btn btn-primary" style="margin-left: auto; margin-right:5px;"><span>Save</span></button>
                            <button class="submit btn btn-primary" style="margin-left: auto"><span>Submit</span></button>
                        </div>
                    </div>
                </form>
            @endif

        @endif

        </div>
        <div class="copy">
            <span>
                <img src="{{asset('img/logo.png')}}" alt="TVRLS" class="footer_logo">                
            </span>
            <span>&copy; 2021 All rights reserved.</span>
        </div>
    </div>
    
@endsection



@push('scripts')
    <style type="text/css">
        .buttonload {            
            border: none;
            display: flex;
            flex-direction: row;
            flex-wrap: nowrap;
            padding-left:5px ;
            align-items: center;
            align-content:center;
            justify-content: center;
            min-width: 100px !important;
        }
        .buttonload  span{
            display: inline-block;
            padding-left:5px;
        }

        .is-invalid{
            border-color: #dc3545 !important;
        }

        .form-full {
            flex-basis: 100% !important;
        }

        .video-container {
            position: relative;
            padding-bottom: 56.25%;
            padding-top: 30px;
            height: 0;
            overflow: hidden;
        }

        .video-container iframe,
        .video-container object,
        .video-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        button{
            width:90px !important;
        }

        @media(max-width:568px) {
            button{
                width:60px !important;
            }
        }
    </style>
    <script>
    @if(!empty($form))
        $(function(){
            let numSteps  = {{$step}};
            let formid = {{$form->id}};
            let hasInput = {{ $hasform?'true':'false' }};
            var timeArray = [0,0];
            let currStep = 1;
            let isfetchable = true;
            let data = [];

            window.timeout = null;


            @if($form->time!=0)
                $('#timer').text({{$form->time}} + ":" + 1);

                function startTimer() {
                    var presentTime = $('#timer').text();
                    timeArray = presentTime.split(/[:]+/);        
                    var m = timeArray[0];
                    var s = (timeArray[1] - 1);
                    s = ((s<0)?'59':(s+'').padStart(2,0));
                    
                    if(m==0 && s==0){
                        submitForm();
                    }
                    if(s==59) m=m-1;
                    if(m<0) return;
                    $('#timer').text(m + ":" + s);
                    timeout = setTimeout(startTimer, 1000);
                }

                startTimer();     
            @endif       

            function disablebutton(){
                if(!hasInput){
                    $('.save, .submit').hide();
                }
            }
            disablebutton();

            if(numSteps==1){
                $('.nav').attr({
                    "style":"display: none !important"
                });

                $('.nav_submit').attr({
                    "style":"display: flex !important"
                });
            }           
            
            

            $('.steps').hide();
            $(`.step_${currStep}`).show();

            function endSurvey(){
                $('.steps').hide();
                $('.nav_submit, .nav, .timer_container').attr({
                    "style":"display: none !important"
                });
                clearTimeout(timeout);
                $('.survey_end').show();
            }

            function pauseSurvey(){
                $('.steps').hide();
                $('.nav_submit, .nav, .timer_container').attr({
                    "style":"display: none !important"
                });
                clearTimeout(timeout);
                $('.survey_pause').show();
            }

            function submitForm(){
                $('.submit').addClass('buttonload').prepend('<i class="fas fa-circle-notch fa-spin"></i>');                
                $('input, textarea, button').prop('disabled', true);
                $('.back').hide();
                let fname = $('#fname').val();
                let lname = $('#lname').val();

                let email = $('#useremail').val();
                if(!fname || !email){
                    endSurvey();
                }

                $(`.steps`).each(function(){
                    let qid = $(this).data('qst');
                    let ans = $(this).find('.ans').val();

                    if(ans){
                        data.push({
                            qid:qid,
                            ans:ans,
                        });
                    }
                });

                $.ajax({
                    url: "{{route('survey.store')}}",
                    method:'POST',
                    data:{
                        "_token": "{{ csrf_token() }}",
                        ans: data,
                        form_id: formid,
                        action: 'submit',
                        user_data:{
                            name: fname.trim()+' '+lname.trim(),
                            email: email,
                            status: 1,
                            timeleft: ((timeArray[0]*60) + (timeArray[1]-1)),
                        }
                    },
                    success:function(res){
                        endSurvey();
                    },
                });
            }           

            function saveForm(){
                let fname = $('#fname').val();
                let lname = $('#lname').val();
                let email = $('#useremail').val();

                if(!fname || !email){
                    alert("Name and email are required to save the form");
                    return;
                }

                $('.save').addClass('buttonload').prepend('<i class="fas fa-circle-notch fa-spin"></i>');
                //$('input, textarea, button').prop('disabled', true);
                //$('.back').hide();

                $(`.steps`).each(function(){
                    let qid = $(this).data('qst');
                    let ans = $(this).find('.ans').val();

                    if(ans){
                        data.push({
                            qid:qid,
                            ans:ans,
                        });
                    }
                });

                $.ajax({
                    url: "{{route('survey.store')}}",
                    method:'POST',
                    data:{
                        "_token": "{{ csrf_token() }}",
                        ans: data,
                        form_id: formid,
                        action: 'save',                        
                        user_data:{
                            name: fname.trim()+' '+lname.trim(),
                            email: email,          
                            status: 0,             
                            timeleft: ((timeArray[0]*60) + (timeArray[1]-1)),
                        }
                    },
                    success:function(res){
                        $('.save').removeClass('buttonload').find('i').remove();
                        //pauseSurvey();
                    },
                });

            }

            function validateForm(){
                let validate = true;
                $('.err').remove();
                $('.is-invalid').removeClass('is-invalid');

                $(`.step_${currStep}`).find('.required').each(function(){
                    if(!($(this).val())){
                        $(this).addClass('is-invalid');
                        $(this).after(`<span class="text-danger err">This field is required</span>`);
                        validate = false;
                    }else if($(this).attr('type')=='email'){
                        if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(this).val()))){
                            $(this).addClass('is-invalid');
                            $(this).after(`<span class="text-danger err">This Email is invalid</span>`);
                            validate = false;
                        }
                    }
                });
                return validate;
            }

            $('body').on('click', '.submit', function(e){
                e.preventDefault();
                if(!validateForm()) return;
                submitForm();
            });

            $('body').on('click', '.save', function(e){
                e.preventDefault();
                if(currStep==1){
                    if(!validateForm()) return;
                }                
                saveForm();
            });

            $('body').on('click', '.next', function(e){
                e.preventDefault();
                if(!validateForm()) return;

                if(currStep==1 && isfetchable){
                    let email = $('#useremail').val();
                    $.ajax({
                        url: "{{route('survey.get')}}",
                        method:'POST',
                        data:{
                            "_token": "{{ csrf_token() }}",
                            form_id: formid,
                            email: email,
                            
                        },
                        success:function(res){
                            if(res.success){
                                let timeleft = res.user_submit.timeleft;
                                m =  Math.floor(timeleft / 60);
                                s =  timeleft - m * 60
                                $('#timer').text(m + ":" + s);
                                res.qst.forEach(ele => {                                
                                    $(`#qst_${ele.question_id}`).val(ele.response);
                                }); 
                                isfetchable = false;
                            }  
                        },
                    });
                }                

                $(`.step_${currStep}`).hide();
                $(`.step_${++currStep}`).fadeIn();
                if(currStep==numSteps){
                    $('.nav').attr({
                        "style":"display: none !important"
                    });;
                    $('.nav_submit').attr({
                        "style":"display: flex !important"
                    });
                }else{
                    $('.nav').attr({
                        "style":"display: flex !important"
                    });;
                    $('.nav_submit').attr({
                        "style":"display: none !important"
                    });
                }

                if(currStep ==1) $('.back').hide();
                else $('.back').show();

                disablebutton();

            });

            $('body').on('click', '.back', function(e){
                e.preventDefault();

                if(currStep==1) return false;
                $(`.step_${currStep}`).hide();

                $('.nav').attr({
                    "style":"display: flex !important"
                });
                $('.nav_submit').attr({
                    "style":"display: none !important"
                });
                $(`.step_${--currStep}`).fadeIn();
                if(currStep ==1) $('.back').hide();
                else $('.back').show();

                disablebutton();
            });
        });

    @endif
    </script>
@endpush
