@extends('layouts.auth')

@section('content')
    <section class="h-100">
        <div class="container h-100">
            <div class="row justify-content-md-center h-100">
                <div class="card-wrapper">
                    <?php /* <div class="brand">
                        <img src="img/logo.jpg" alt="logo">
                    </div> */ ?>

                    <div class="card fat mt-5">
                        <div class="card-body">
                            <h4 class="card-title">Login</h4>
                                {!! Form::open(['route' => 'login','Method'=>'POST','class'=>'my-login-validation']) !!}
                                @if($errors->any())
                                    @foreach ($errors->all() as $error)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $error }}</strong>
                                        </span>
                                        @endforeach
                                @endif

                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    <input id="email" type="email" class="form-control @if($errors->any()) is-invalid @endif" name="email" value="{{ old('email') }}" required autofocus >
                                    <div class="invalid-feedback">
										Email is invalid
									</div>
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" type="password" class="form-control @if($errors->any()) is-invalid @endif" name="password" required data-eye autocomplete="current-password">
                                    <div class="invalid-feedback">
								    	Password is required
							    	</div>
                                </div>

                                <div class="form-group">
                                    <div class="custom-checkbox custom-control">
                                        <input type="checkbox" name="remember" id="remember" class="custom-control-input"  {{ old('remember') ? 'checked' : '' }}>
                                        <label for="remember" class="custom-control-label">Remember Me</label>
                                    </div>
                                </div>

                                <div class="form-group m-0">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        Login
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="footer">
                        Copyright &copy; {{ date('Y') }} - Catalyst
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/my-login.css') }}">
    <script src="{{ asset('js/my-login.js') }}" defer></script>
@endpush
