<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTypesTable extends Migration
{
    public function up()
    {
        Schema::create('questions_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question_type', 50)->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('questions_types');
    }
}
