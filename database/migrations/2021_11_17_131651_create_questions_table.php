<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('question_text');
            $table->tinyInteger('question_type')->comment('1->text, 2->textarea, 3->paragraph, 4->image, 5->pagebreak');
            $table->string('placeholder_text', 30)->default('');
            $table->bigInteger('form_id')->unsigned();
            $table->tinyInteger('required')->default(0)->comment('1 - Yes, 0 - No');;
            $table->softDeletes();
            $table->timestamps();

        });
    }
    
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
