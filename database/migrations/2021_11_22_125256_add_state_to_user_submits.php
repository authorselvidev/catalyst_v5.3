<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStateToUserSubmits extends Migration
{
    public function up()
    {
        Schema::table('user_submits', function (Blueprint $table) {
            $table->tinyInteger('status')->nullable()->after('form_id');
            $table->Integer('timeleft')->unsigned()->nullable()->after('status');
        });
    }
    
    public function down()
    {
        Schema::table('user_submits', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('timeleft');
        });
    }
}
