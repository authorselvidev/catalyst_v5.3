<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Admin',
            'email'=>'info@ascendus.com',
            'role'=>1,
            'password'=> Hash::make('admin@123'),
            ]
        );
    }
}
